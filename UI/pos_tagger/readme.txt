Install python3, pip, virtualenv

[
// if required, install python3-dev
sudo apt-get install python3-dev
]

--------------------------------------------------------------

// Initial setup
// inside the directory UI/pos_tagger/

virtualenv -p python3 venv
source venv/bin/activate

pip install -r requirements.txt

cd pos
python manage.py runserver

// visit http://127.0.0.1:8000/tagger_v0/ with your Web browser


// Initial setup

--------------------------------------------------------------

// Run the project
// inside the directory UI/pos_tagger/

source venv/bin/activate

cd pos
python manage.py runserver

// visit http://127.0.0.1:8000/tagger_v0/ with your Web browser


// Run the project