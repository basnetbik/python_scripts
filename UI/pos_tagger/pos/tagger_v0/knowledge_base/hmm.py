from __future__ import division
from collections import defaultdict
import string
import operator
from math import log


class HMM(object):
    ADDITIVE_SMOOTHING = 10 ** -100

    def __init__(self, n=26, m=26, states=list(string.ascii_lowercase), observations=list(string.ascii_lowercase)):
        self.N, self.M = n, m   # N: no of states, M: no of observations
        self.STATES, self.OBSERVATIONS = states, observations
        self.PI = defaultdict(float)   # state start probabilities
        self.B = defaultdict(lambda: defaultdict(float))   # state observation probabilities
        self.A = defaultdict(lambda: defaultdict(float))   # state transition probabilities

    def get_pi(self, state):
        try:
            return self.PI[state] + self.ADDITIVE_SMOOTHING
        except KeyError:
            return 0 + self.ADDITIVE_SMOOTHING

    def get_a(self, current_state, next_state):
        try:
            return self.A[current_state][next_state] + self.ADDITIVE_SMOOTHING
        except KeyError:
            return 0 + self.ADDITIVE_SMOOTHING

    def get_b(self, state, observation):
        try:
            return self.B[state][observation] + self.ADDITIVE_SMOOTHING
        except KeyError:
            return 0 + self.ADDITIVE_SMOOTHING

    def train(self, state_sequences, observation_sequences):
        """
        Trains HMM using MLE
        """
        number_of_training_sequences = len(state_sequences)
        state_observation_count = defaultdict(float)
        state_transition_count = defaultdict(float)
        for state_seq, observation_seq in zip(state_sequences, observation_sequences):
            if not state_seq or not observation_seq:
                continue

            self.PI[state_seq[0]] += 1

            for state, observation in zip(state_seq, observation_seq):
                self.B[state][observation] += 1
                state_observation_count[state] += 1

            for current_state, next_state in zip(state_seq, state_seq[1:]+['$']):
                if next_state != '$':
                    self.A[current_state][next_state] += 1
                    state_transition_count[current_state] += 1

        self.PI = {
            state: count/number_of_training_sequences for state, count in self.PI.items()
        }

        self.B = {
            state: {
                observation_: count/state_observation_count[state] for observation_, count in observation.items()
            } for state, observation in self.B.items()
        }

        self.A = {
            current_state: {
                next_state_: count/state_transition_count[current_state] for next_state_, count in next_state.items()
            } for current_state, next_state in self.A.items()
        }

    def viterbi(self, observation_sequence):
        observation_sequence = list(observation_sequence)
        delta = defaultdict(lambda: defaultdict(float))
        psi = defaultdict(dict)

        i = 0
        for state in self.STATES:
            delta[i][state] = log(self.get_pi(state)) + log(self.get_b(state, observation_sequence[0]))
            psi[i][state] = '0'

        for i in range(1, len(observation_sequence)):
            for state in self.STATES:
                possible_transitions = [
                    delta[i-1][prev_state] + log(self.get_a(prev_state, state))
                    for prev_state in self.STATES
                ]

                delta[i][state] = max(possible_transitions)
                psi[i][state] = self.STATES[possible_transitions.index(delta[i][state])]
                delta[i][state] = delta[i][state] + log(self.get_b(state, observation_sequence[i]))

        final_state = sorted(delta[i].items(), key=operator.itemgetter(1), reverse=True)[0][0]
        best_state_sequence = [final_state]
        traversing_state = final_state

        while i > 0:
            prev_state = psi[i][traversing_state]
            best_state_sequence = [prev_state] + best_state_sequence
            traversing_state = prev_state
            i -= 1

        return best_state_sequence
