from django.apps import AppConfig


class TaggerV0Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tagger_v0'
