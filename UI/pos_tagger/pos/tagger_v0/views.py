import pickle
import os
import sys

from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


module_dir = os.path.dirname(__file__)
file_path = os.path.join(module_dir, 'knowledge_base/POS_TAGGER.pickle')

sys.path.append('./tagger_v0/knowledge_base/')


with open(file_path, 'rb') as trained_model:
    trained_hmm = pickle.load(trained_model)
    hmm = trained_hmm['trained_hmm']


def index(request):
    return render(request, 'tagger_v0/index.html')


@csrf_exempt
def tag(request):
    sentence = request.POST.get('sentence', '').lower()
    sentence = sentence.split(' ')

    predicted_tags = hmm.viterbi(sentence)
    predicted_tags = '\n'.join(
        [
            f'{item} ({tag})'
            for item, tag in zip(sentence, predicted_tags)
        ]
    )

    return JsonResponse(
        {'result': predicted_tags}
    )
