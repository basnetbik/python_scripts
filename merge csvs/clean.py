import re
from os.path import join
import glob

n = 0


def no(o):
    global n
    m = n
    n += 1
    return '{'+str(m)+'}'

csv_paths = ['data', 'data_cleaned']

for i, csv_file in enumerate(glob.glob(join(csv_paths[0], "*.csv"))):
    n = 0
    _file = open(csv_file, 'r').read()
    _file = re.sub(r'\n\(', '(', _file)
    _file = re.sub(r'CAT,.,.*\.",Close,', '', _file)
    reg_dates = re.findall(r'\d+[^\(]\(\d+ reg\)', _file)
    reg_dates = map(lambda _date: ' ('.join([d.strip() for d in _date.split('(')]), reg_dates)
    _file = re.sub(r'\}', 'RIGHTBRACKET', _file)
    _file = re.sub(r'\{', 'LEFTBRACKET', _file)
    _file = re.sub(r'\d+[^\(]\(\d+ reg\)', no, _file)
    _file = _file.format(*reg_dates)
    _file = re.sub('RIGHTBRACKET', '}', _file)
    _file = re.sub('LEFTBRACKET', '{', _file)

    with open(join(csv_paths[1], csv_file.split('/')[1]), 'w') as fp:
        fp.write(_file)