import datetime
import glob
from os.path import join
from pandas import read_csv, concat, DataFrame
import re

columns = {
    0: 'Link',
    1: 'Car',
    3: 'Year',
    4: 'Type',
    5: 'Miles',
    6: 'Transmission',
    7: 'Engine'
}

columns_list = ['Link', 'Car', 'Year', 'Type', 'Miles', 'Transmission', 'Engine']
dates_list = set()


def df_from_csv(path):
    temp_df = read_csv(path, error_bad_lines=False, engine='c', header=None)
    current_date = '%s/%s/%s' % re.findall(r'[^\d]*(\d+)[^\d]*(\d+)[^\d]*(\d+)[^\d]*', path)[0]
    temp_df[8] = current_date
    dates_list.add(current_date)
    return temp_df


csv_path = 'data_cleaned'
list_ = []

for i, csv_file in enumerate(glob.glob(join(csv_path, "*.csv"))):
    try:
        df = df_from_csv(csv_file)
    except Exception, e:
        continue
    else:
        list_.append(df)

frame = concat(list_)
frame = frame.dropna()

for i in xrange(1, 8):
    frame[i] = frame[i].map(str).map(str.strip).map(str.lower)

# frame[6] = frame[6].map(str.lower)

list_ = []


def merge_groups(group):
    temp_df = {}
    for index, row in group.iterrows():
        for i in [0, 1, 3, 4, 5, 6, 7]:
            if columns[i] not in temp_df:
                temp_df[columns[i]] = row[i]

        if row[8] not in temp_df:
            temp_df[row[8]] = row[2]

    list_.append(temp_df)
    return temp_df

frame_groups = frame.groupby(by=[1, 3, 4, 5, 6, 7])
frame_groups.apply(merge_groups)

dates_list = sorted(dates_list, key=lambda d: datetime.datetime.strptime(d, '%d/%m/%Y'))
frame = DataFrame(list_)
frame.to_csv('o.csv', columns=columns_list+list(dates_list))