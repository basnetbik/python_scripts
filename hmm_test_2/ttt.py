from collections import defaultdict
import pickle

from hmm import HMM, baum_welch, forward


class TicTacToe(object):
    def __init__(self):
        self.board = [[' ', ' ', ' '] for _ in range(3)]
        self.player = 'X'
        self.track_moves = defaultdict(list)
        self.move_count = 0

    def draw_board(self):
        for i, row in enumerate(self.board):
            print(' | '.join(row))
            if i != 2:
                print('-' * 10)

    def possible_spaces(self):
        spaces = []
        for i, row in enumerate(self.board):
            for j, cell in enumerate(row):
                if self.board[i][j] == ' ':
                    spaces.append((i, j))

        return spaces

    def switch_player(self):
        if self.player == 'X':
            self.player = '0'
        else:
            self.player = 'X'

    def flattened_board(self):
        result = []
        for row in self.board:
            for cell in row:
                result.append(str(cell))

        return result

    def move(self, r, c):
        self.board[r][c] = self.player
        self.move_count += 1
        self.track_moves[self.player].append(self.flattened_board())
        self.draw_board()
        win = self.check_win()
        if not win:
            if self.move_count == 9:
                return 'DRAW'

            self.switch_player()

        return win

    def check_win(self):
        if any([
            self.board[0][0] == self.board[0][1] == self.board[0][2] == self.player,
            self.board[1][0] == self.board[1][1] == self.board[1][2] == self.player,
            self.board[2][0] == self.board[2][1] == self.board[2][2] == self.player,
            self.board[0][0] == self.board[1][0] == self.board[2][0] == self.player,
            self.board[0][1] == self.board[1][1] == self.board[2][1] == self.player,
            self.board[0][2] == self.board[1][2] == self.board[2][2] == self.player,
            self.board[0][0] == self.board[1][1] == self.board[2][2] == self.player,
            self.board[0][2] == self.board[1][1] == self.board[2][0] == self.player,
        ]):
            return True

        return False

    def user_move(self):
        possible_spaces = self.possible_spaces()
        while True:
            move = int(input('0-8? '))
            r = move // 3
            c = move - r*3

            if (r, c) in possible_spaces:
                move_status = self.move(r, c)
                if move_status:
                    print('User won')
                    return True
                elif move_status == 'DRAW':
                    return 'DRAW'

                break
            else:
                print('Invalid move')

    def random_move(self, hmm):
        possible_spaces = self.possible_spaces()
        import random
        r, c = random.choice(possible_spaces)

        move_status = self.move(r, c)
        if move_status:
            print('HMM won')
            return True
        elif move_status == 'DRAW':
            return 'DRAW'

    def hmm_move(self, hmm):
        possible_spaces = self.possible_spaces()

        max_likelihood = -float('inf')
        r_max = c_max = None

        for r, c in possible_spaces:
            self.board[r][c] = self.player

            current_board = self.flattened_board()
            for i in range(len(current_board)):
                if current_board[i] == ' ':
                    continue

                if current_board[i] == self.player:
                    current_board[i] = '1'
                else:
                    current_board[i] = '-1'

            prob = forward(hmm, current_board, scaling=True)[0]
            if prob > max_likelihood:
                max_likelihood = prob
                r_max, c_max = r, c

            self.board[r][c] = ' '

        move_status = self.move(r_max, c_max)
        if move_status:
            print('HMM won')
            return True
        elif move_status == 'DRAW':
            return 'DRAW'


class Game(object):
    """
    Basic HMM based TTT learner that learns from the opponents it plays with.
    """
    def __init__(self):
        self.hmm = self.get_hmm()
        self.ttt = TicTacToe()
        self.ttt.draw_board()

    def play(self):
        while True:
            move_status = self.ttt.user_move()
            if move_status:
                break

            # if self.ttt.random_move():
            move_status = self.ttt.hmm_move(self.hmm)
            if move_status:
                break

        if move_status is True:
            self.train_hmm()

    def get_hmm(self):
        try:
            with open('TTT.pickle', 'rb') as trained_model:
                trained_hmm = pickle.load(trained_model)
                return trained_hmm.get('model')
        except FileNotFoundError:
            return HMM(n_states=5, V=[' ', '1', '-1'])

    def train_hmm(self):
        train_sequences = self.ttt.track_moves[self.ttt.player]
        for train_sequence in train_sequences:
            for i in range(len(train_sequence)):
                if train_sequence[i] == ' ':
                    continue

                if train_sequence[i] == self.ttt.player:
                    train_sequence[i] = '1'
                else:
                    train_sequence[i] = '-1'

        trained_hmm = baum_welch(hmm=self.hmm, Obs_seqs=train_sequences, epochs=50)

        with open('TTT.pickle', 'wb') as trained_model:
            pickle.dump(dict(model=trained_hmm), trained_model)


game = Game()
game.play()
