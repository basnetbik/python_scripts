Given a list of dictionary words and a grid or matrix of alphabets, find all the words in the dictionary that can be
formed by starting from any grid location and travelling the grid in a way that each grid position is connected to its
eight neighbors (right, left, up, down, 4 diagonals). While moving through the grid from a starting location,
a single word can visit any grid space only once.


word list:  https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english.txt