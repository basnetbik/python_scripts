import pickle
import string
import random

random.seed(10)


with open('knowledge_base', 'rb') as knowledge_base_file:
    knowledge_base_dict = pickle.load(knowledge_base_file)
    knowledge_base = knowledge_base_dict.get('knowledge_base', {})
    word_base = knowledge_base_dict.get('word_base', {})

test_grid = []

number_of_rows = 150
number_of_columns = 100

possible_alphabets = tuple(string.ascii_lowercase)
number_of_possible_alphabets = len(possible_alphabets)

for row in range(number_of_rows):
    test_grid.append(['']*number_of_columns)

    for column in range(number_of_columns):
        random_position = random.randint(0, number_of_possible_alphabets-1)
        test_grid[row][column] = possible_alphabets[random_position]

# SEARCHING
collected_words = dict()


def search(partial_word_, row_, column_):
    partial_word_ = str(partial_word_)

    neighbors = list()

    new_row = row_ - 1
    if new_row >= 0:
        neighbors.append((new_row, column_))

    new_row = row_ + 1
    if new_row < number_of_rows:
        neighbors.append((new_row, column_))

    new_column = column_ - 1
    if new_column >= 0:
        neighbors.append((row_, new_column))

    new_column = column_ + 1
    if new_column < number_of_columns:
        neighbors.append((row_, new_column))

    new_row = row_ - 1
    new_column = column_ - 1
    if new_row >= 0 and new_column >= 0:
        neighbors.append((new_row, new_column))

    new_row = row_ - 1
    new_column = column_ + 1
    if new_row >= 0 and new_column < number_of_columns:
        neighbors.append((new_row, new_column))

    new_row = row_ + 1
    new_column = column_ - 1
    if new_row < number_of_rows and new_column >= 0:
        neighbors.append((new_row, new_column))

    new_row = row_ + 1
    new_column = column_ + 1
    if new_row < number_of_rows and new_column < number_of_columns:
        neighbors.append((new_row, new_column))

    for neighbor in neighbors:
        current_partial_word = partial_word_ + test_grid[neighbor[0]][neighbor[1]]

        if (current_partial_word in word_base) and current_partial_word not in collected_words:
            collected_words[current_partial_word] = True

        if current_partial_word in knowledge_base:
            search(current_partial_word, neighbor[0], neighbor[1])


for row in range(number_of_rows):
    for column in range(number_of_columns):
        partial_word = test_grid[row][column]

        if (partial_word in word_base) and partial_word not in collected_words:
            collected_words[partial_word] = True

        if partial_word in knowledge_base:
            search(partial_word, row, column)


collected_words = list(collected_words.keys())
collected_words.sort()
print(len(collected_words), ' words found.')
print(list(collected_words))


