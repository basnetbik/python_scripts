import pickle

with open('google-10000-english.txt', 'r') as word_list_file:
    word_list = sorted(word_list_file.read().split('\n'), key=lambda word: len(word))
    len_of_longest_word = len(word_list[-1])

knowledge_base = dict()
word_base = dict()

for word in word_list:
    word_base[word] = True
    current_word = word[:-1]
    for i in range(len(current_word)):
        partial_word = current_word[:i+1]
        if partial_word not in knowledge_base:
            knowledge_base[partial_word] = True


with open('knowledge_base', 'wb') as knowledge_base_file:
    pickle.dump(dict(knowledge_base=knowledge_base, word_base=word_base), knowledge_base_file)
