from collections import defaultdict
from functools import reduce
import random
import string

random.seed(10)


with open('google-10000-english.txt', 'r') as word_list_file:
    word_list = word_list_file.read().split('\n')


# create a prefix tree
Trie = lambda: defaultdict(Trie)
trie = Trie()

for word in word_list:
    reduce(dict.__getitem__, word, trie)['END'] = True


# create a test grid
test_grid = []

number_of_rows = 150
number_of_columns = 100

possible_alphabets = tuple(string.ascii_lowercase)
number_of_possible_alphabets = len(possible_alphabets)

for row in range(number_of_rows):
    test_grid.append(['']*number_of_columns)

    for column in range(number_of_columns):
        random_position = random.randint(0, number_of_possible_alphabets-1)
        test_grid[row][column] = possible_alphabets[random_position]


# SEARCHING
collected_words = set()

possible_spaces = (
    (1, 0), (-1, 0), (0, 1), (0, -1),
    (1, 1), (-1, 1), (1, -1), (-1, -1)
)


def search(partial_word_, row_, column_, partial_trie_):
    neighbors = filter(
        lambda space: 0 <= space[0] < number_of_rows and 0 <= space[1] < number_of_columns,
        map(
            lambda space: (row_+space[0], column_+space[1]),
            possible_spaces
        )
    )

    for neighbor in neighbors:
        alphabet = test_grid[neighbor[0]][neighbor[1]]

        if alphabet in partial_trie_:
            temp_trie = partial_trie_[alphabet]

            current_partial_word = partial_word_ + alphabet

            if ('END' in temp_trie) and current_partial_word not in collected_words:
                collected_words.add(current_partial_word)

            search(current_partial_word, neighbor[0], neighbor[1], temp_trie)


for row in range(number_of_rows):
    for column in range(number_of_columns):
        partial_word = test_grid[row][column]

        if partial_word in trie:
            partial_trie = trie[partial_word]

            if ('END' in partial_trie) and partial_word not in collected_words:
                collected_words.add(partial_word)

            search(partial_word, row, column, partial_trie)


collected_words = list(collected_words)
collected_words.sort()
print(len(collected_words), ' words found.')
print(list(collected_words))
