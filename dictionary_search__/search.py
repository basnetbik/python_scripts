"""
Given a string consisting of lowercase alphabets, and a list of dictionary words,
find the sequences of words that can be formed by adding spaces in between any consecutive alphabets.
"""

from collections import defaultdict
from functools import reduce


test_string = 'fishdoesnotflyundermostcircumstances'


with open('google-10000-english.txt', 'r') as word_list_file:
    word_list = word_list_file.read().split('\n')

# create a prefix tree

Trie = lambda: defaultdict(Trie)
trie = Trie()

for word in word_list:
    reduce(dict.__getitem__, word, trie)['END'] = True


def search(i, result):
    if i == len(test_string):
        print(result)
        return

    prefix_tree = trie
    while i < len(test_string) and test_string[i] in prefix_tree:
        prefix_tree = prefix_tree[test_string[i]]
        result += test_string[i]

        i += 1
        if 'END' in prefix_tree:
            search(i, result + ' ')


search(0, '')
