"""
Given a string consisting of lowercase alphabets, and the brown corpus data,
find the sequences of words that can be formed by adding spaces in between any consecutive alphabets.
"""

from collections import defaultdict
from functools import reduce
import pickle
import pprint


test_string = "givenastringconsistingoflowercaseletters"


with open('kbase.pickle', 'rb') as kbase:
    kbase = pickle.load(kbase)
    words = kbase['words']
    # https://en.wikipedia.org/wiki/N-gram
    state_bigram = kbase['state_pairs']

with open('words.txt', 'r') as word_list_file:
    word_list = word_list_file.read().split('\n')


# create a prefix tree
Trie = lambda: defaultdict(Trie)
trie = Trie()

for word in word_list:
    reduce(dict.__getitem__, word, trie)['END'] = True


# check bigram pair
def check_pair(word1, word2):
    for state1 in words[word1]:
        for state2 in words[word2]:
            if (state1, state2) in state_bigram:
                return True

    return False


# search
sequences = []

def search(i, result, prev_word):
    if i == len(test_string):
        sequences.append(result)
        return

    prefix_tree = trie
    temp = ''
    while i < len(test_string) and test_string[i] in prefix_tree:
        prefix_tree = prefix_tree[test_string[i]]
        temp += test_string[i]

        i += 1
        if 'END' in prefix_tree:
            if not prev_word or check_pair(prev_word, temp):    # try commenting this line
                search(i, result + temp + ' ', temp)


search(0, '', '')

print(len(sequences), ' sequences found. \n')
pprint.pprint(sequences)
