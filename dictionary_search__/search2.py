"""
Given a string consisting of lowercase alphabets, and the brown corpus data,
find the sequences of words that can be formed by adding spaces in between any consecutive alphabets.
"""

from collections import defaultdict
from functools import reduce
import pickle
import pprint


test_string = "givenastringconsistingoflowercaseletters"


with open('kbase.pickle', 'rb') as kbase:
    kbase = pickle.load(kbase)
    words = kbase['words']
    # https://en.wikipedia.org/wiki/N-gram
    state_ngram = kbase['state_pairs']

with open('words.txt', 'r') as word_list_file:
    word_list = word_list_file.read().split('\n')


# create a prefix tree
Trie = lambda: defaultdict(Trie)
trie = Trie()

for word in word_list:
    reduce(dict.__getitem__, word, trie)['END'] = True


# check bigram pair
def check_bigram(pair_words):
    word1, word2 = pair_words
    for state1 in words[word1]:
        for state2 in words[word2]:
            if (state1, state2) in state_ngram:
                return True

    return False

# check trigram
def check_trigram(pair_words):
    if len(pair_words) == 2:
        return check_bigram(pair_words)

    word0, word1, word2 = pair_words
    for state0 in words[word0]:
        for state1 in words[word1]:
            for state2 in words[word2]:
                if (state0, state1, state2) in state_ngram:
                    return True

    return False

# search
sequences = []

def search(i, result, prev_word):
    if i == len(test_string):
        sequences.append(result)
        return

    prefix_tree = trie
    temp = ''
    while i < len(test_string) and test_string[i] in prefix_tree:
        prefix_tree = prefix_tree[test_string[i]]
        temp += test_string[i]

        i += 1
        if 'END' in prefix_tree:
            pairs = prev_word+[temp]
            if not prev_word or check_trigram(pairs):
                if len(pairs) == 3:
                    pairs.pop(0)

                search(i, result + temp + ' ', pairs)


search(0, '', [])

print(len(sequences), ' sequences found. \n')
pprint.pprint(sequences)
