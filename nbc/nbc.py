# NAIVE BAYES CLASSIFIER
from __future__ import division
from collections import defaultdict
import glob
import re
import operator
from math import log

GENRES = {
    "a": "A. PRESS: REPORTAGE",
    "b": "B. PRESS: EDITORIAL",
    "c": "C. PRESS: REVIEWS",
    "d": "D. RELIGION",
    "e": "E. SKILL AND HOBBIES",
    "f": "F. POPULAR LORE",
    "g": "G. BELLES-LETTRES",
    "h": "H. MISCELLANEOUS: GOVERNMENT & HOUSE ORGANS",
    "j": "J. LEARNED",
    "k": "K: FICTION: GENERAL",
    "l": "L: FICTION: MYSTERY",
    "m": "M: FICTION: SCIENCE",
    "n": "N: FICTION: ADVENTURE",
    "p": "P. FICTION: ROMANCE",
    "r": "R. HUMOR"
}

genre_training_instances = dict()
total_training_instances = 0
likelihood = defaultdict(dict)

stop_words = open('stop_words.txt', 'r').read().split(',')

for genre in GENRES.keys():
    current_genre_docs_paths = glob.glob('brown/c%s*' % genre)
    genre_training_instances[genre] = len(current_genre_docs_paths)
    total_training_instances += genre_training_instances[genre]
    current_genre_words = defaultdict(int)
    current_genre_training_instances = 0
    for current_genre_doc_path in current_genre_docs_paths:
        current_genre_doc = open(current_genre_doc_path, 'r').read()

        current_genre_doc_words = re.findall(r'(\w+)/', current_genre_doc)
        for word in current_genre_doc_words:
            if word in stop_words:
                continue
            current_genre_words[word.lower()] += 1
            current_genre_training_instances += 1

    for word in current_genre_words:
        likelihood[genre][word] = current_genre_words[word]/current_genre_training_instances


ADDITIVE_SMOOTHING = 10 ** -100

prior = dict()
for genre in GENRES.keys():
    prior[genre] = genre_training_instances[genre]/total_training_instances


def find_likelihood(genre_, word_):
    try:
        return likelihood[genre_][word_] + ADDITIVE_SMOOTHING
    except KeyError:
        return 0 + ADDITIVE_SMOOTHING


def posterior(data, genres_considered=3, display=True):
    test_instances = re.findall(r'\w+', data)
    test_instances = filter(lambda _: not(_ in stop_words), test_instances)
    results = defaultdict(float)

    for GENRE in GENRES.keys():
        genre_log_likelihood = log(prior[GENRE])

        for test_instance in test_instances:
            genre_log_likelihood += log(find_likelihood(GENRE, test_instance.lower()))

        results[GENRES[GENRE]] = genre_log_likelihood

    results = sorted(results.items(), key=operator.itemgetter(1), reverse=True)
    if display:
        for result in results[:genres_considered]:
            print result

    return results


# test
quotes = open('quotes.txt', 'r').read().split('\n')
sample_size = len(quotes)   # 36166
for quote in quotes[:100]:
    print quote
    posterior(quote, genres_considered=3)
    print