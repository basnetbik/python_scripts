"""
Given one dimensional matrix with n elements,
find the most square two dimensional matrix with all the elements of the given matrix and at most m spaces
: return the dimension of the thus formed matrix
"""
import math

n = 99
m = 3


def find_square_factors(dim):
    for dim1 in range(int(math.sqrt(dim)), 0, -1):
        if dim % dim1 == 0:
            factor1 = dim/dim1
            factor2 = dim/factor1

            return factor1, factor2, abs(factor1-factor2)


def find_dimensions(dimension, number_of_spaces):
    current_dims = (None, None, float('inf'))

    for dim in range(dimension+number_of_spaces, dimension-1, -1):
        current_dimensions = find_square_factors(dim)
        if current_dimensions[2] == 0:
            return current_dimensions[:2]

        if current_dimensions[2] < current_dims[2]:
            current_dims = current_dimensions

    return current_dims[:2]

print(find_dimensions(n, m))
