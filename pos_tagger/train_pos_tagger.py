from __future__ import division
import glob
import pickle
from random import randint

from hmm import HMM
from tags import *

GENRES = {
    "a": "A. PRESS: REPORTAGE",
    "b": "B. PRESS: EDITORIAL",
    "c": "C. PRESS: REVIEWS",
    "d": "D. RELIGION",
    "e": "E. SKILL AND HOBBIES",
    "f": "F. POPULAR LORE",
    "g": "G. BELLES-LETTRES",
    "h": "H. MISCELLANEOUS: GOVERNMENT & HOUSE ORGANS",
    "j": "J. LEARNED",
    "k": "K: FICTION: GENERAL",
    "l": "L: FICTION: MYSTERY",
    "m": "M: FICTION: SCIENCE",
    "n": "N: FICTION: ADVENTURE",
    "p": "P. FICTION: ROMANCE",
    "r": "R. HUMOR"
}

train_state_sequences, test_state_sequences = list(), list()
train_observation_sequences, test_observation_sequences = list(), list()
states, observations = set(), set()

PERCENTAGE_OF_TRAIN_DATA = 80
GENRES_CONSIDERED = len(GENRES)   # len(GENRES) = 15


print 'parsing data start\n'

for genre in GENRES.keys()[:GENRES_CONSIDERED]:
    current_genre_docs_paths = glob.glob('brown/c%s*' % genre)
    for current_genre_doc_path in current_genre_docs_paths:
        current_genre_doc = open(current_genre_doc_path, 'r').read().lower()
        sentences = filter(None, current_genre_doc.split('\n'))
        for sentence in sentences:
            cleaned_sentence = sentence.replace('\t', '')
            data = re.findall(r'([^/]+)/([^ ]+)', cleaned_sentence)

            probability = randint(0, 100)
            if probability < PERCENTAGE_OF_TRAIN_DATA:
                train_state_sequences.append([train_datum[1].strip() for train_datum in data])
                train_observation_sequences.append([train_datum[0].strip() for train_datum in data])

                states = states.union(set(train_state_sequences[-1]))
                observations = observations.union(set(train_observation_sequences[-1]))
            else:
                test_state_sequences.append([train_datum[1].strip() for train_datum in data])
                test_observation_sequences.append([train_datum[0].strip() for train_datum in data])

print 'parsing data end\n'

states = list(states)
observations = list(observations)

hmm = HMM(len(states), len(observations), states, observations)

print 'train start\n'
hmm.train(train_state_sequences, train_observation_sequences)
print 'train end\n'

print 'save trained model start\n'
with open('POS_TAGGER.pickle', 'wb') as trained_model:
    pickle.dump(dict(
        train_state_sequences=train_state_sequences,
        test_state_sequences=test_state_sequences,
        train_observation_sequences=train_observation_sequences,
        test_observation_sequences=test_observation_sequences,
        trained_hmm=hmm
    ), trained_model)
print 'save trained model end\n'

# test on train instance
test_index = 100

predicted_tags = hmm.viterbi(train_observation_sequences[test_index])

print '(word | predicted tag | actual tag)\n\n'
for word, predicted_tag, actual_tag in zip(
        train_observation_sequences[test_index],
        [get_tag_name(tag_) for tag_ in predicted_tags],
        [get_tag_name(tag_) for tag_ in train_state_sequences[test_index]]
):
    print '(', word, ' | ', predicted_tag, ' | ', actual_tag, ')'
