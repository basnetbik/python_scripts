import pickle
from random import randint

from tags import *


with open('POS_TAGGER.pickle', 'rb') as trained_model:
    trained_hmm = pickle.load(trained_model)

test_observation_sequences = trained_hmm['test_observation_sequences']
test_state_sequences = trained_hmm['test_state_sequences']
train_observation_sequences = trained_hmm['train_observation_sequences']
train_state_sequences = trained_hmm['train_state_sequences']
hmm = trained_hmm['trained_hmm']


def test_all():
    # test on test data
    print '\ntest on test data'
    correctly_tagged_words = 0
    total_words = 0
    for index, test_observation_sequence in enumerate(test_observation_sequences):
        predicted_tags = hmm.viterbi(test_observation_sequence)
        evaluated_predictions = map(
            lambda (predicted_tag_, actual_tag_): predicted_tag_ == actual_tag_,
            zip(
                [get_tag_name(tag_) for tag_ in predicted_tags],
                [get_tag_name(tag_) for tag_ in test_state_sequences[index]]
            )
        )

        total_words += len(evaluated_predictions)
        correctly_tagged_words += sum(evaluated_predictions)
        print 'correct/total-->%d/%d' %(correctly_tagged_words, total_words)

    print 'correctly predicted %d out of %d tags' % (correctly_tagged_words, total_words)

    # test on train data
    print '\ntest on train data\n'
    correctly_tagged_words = 0
    total_words = 0
    for index, train_observation_sequence in enumerate(train_observation_sequences):
        predicted_tags = hmm.viterbi(train_observation_sequence)
        evaluated_predictions = map(
            lambda (predicted_tag_, actual_tag_): predicted_tag_ == actual_tag_,
            zip(
                [get_tag_name(tag_) for tag_ in predicted_tags],
                [get_tag_name(tag_) for tag_ in train_state_sequences[index]]
            )
        )

        total_words += len(evaluated_predictions)
        correctly_tagged_words += sum(evaluated_predictions)
        print 'correct/total-->%d/%d' %(correctly_tagged_words, total_words)

    print 'correctly predicted %d out of %d tags' % (correctly_tagged_words, total_words)


def test_one(test_index=100, test_with='test'):
    # test on instance
    print '\ntest on %s instance' % test_with

    (test_observation_sequence, test_state_sequence) = (
        test_observation_sequences[test_index],
        test_state_sequences[test_index]
    ) if test_with == 'test' else (
        train_observation_sequences[test_index],
        train_state_sequences[test_index]
    )

    predicted_tags = hmm.viterbi(test_observation_sequence)

    print '(word | predicted tag | actual tag)\n\n'
    for word, predicted_tag, actual_tag in zip(
            test_observation_sequence,
            [get_tag_name(tag_) for tag_ in predicted_tags],
            [get_tag_name(tag_) for tag_ in test_state_sequence]
    ):
        print '(', word, ' | ', predicted_tag, ' | ', actual_tag, ')'


test_one(randint(0, len(test_observation_sequences)))
test_one(randint(0, len(train_observation_sequences)), 'train')

test_all()
