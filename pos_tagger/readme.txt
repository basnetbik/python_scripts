train_pos_tagger - trains the hmm with brown corpus, and saves the trained hmm for pos tagging to POS_TAGGER.pickle
test_pos_tagger - tests the trained hmm saved in POS_TAGGER.pickle

for speedup: use numpy and try to do vectorized computation as much as possible