import pickle

with open('knowledge_base', 'rb') as knowledge_base_file:
    knowledge_base_dict = pickle.load(knowledge_base_file)
    knowledge_base = knowledge_base_dict.get('knowledge_base', {})
    word_base = knowledge_base_dict.get('word_base', {})


collected_words = dict()


def search(word, n):
    global collected_words
    collected_words = dict()

    word = str(word)

    if word in word_base:
        collected_words[word] = True

    last_alphabet = word[0]

    is_repeating = False
    repetitions = list()
    repeating_alphabet = None
    start_index = -3

    for i in range(1, len(word)):
        if last_alphabet == word[i]:
            if not is_repeating:
                is_repeating = True
                start_index = i-1
                repeating_alphabet = word[i]
        else:
            if is_repeating:
                is_repeating = False
                end_index = i
                if end_index - start_index >= n:
                    repetitions.append((repeating_alphabet, start_index, end_index))

        last_alphabet = word[i]

    for repetition in repetitions:
        start_index = repetition[1]
        end_index = repetition[2]
        repetition_count = end_index - start_index

        current_partial_word = word[:start_index+1]
        if current_partial_word not in knowledge_base:
            return

        temp_word = list(word)
        for i in range(repetition_count-n):
            temp_word.pop(start_index)
            current_word = ''.join(temp_word)
            if current_word in word_base:
                collected_words[current_word] = True


search('gooooogle', 2)
print(collected_words)

search('gooooogllllle', 3)
print(collected_words)