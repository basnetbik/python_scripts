given a search word, assuming that the query might have been mistyped with
"continuous repetitions of certain alphabet" being the common mistake while typing,
find all the words in the given dictionary the result of mistyping which could have resulted out the
provided search word

n is the number of repetitions (continuous) considered.
also, it is assumed that only one spot in the word contains repetitions.