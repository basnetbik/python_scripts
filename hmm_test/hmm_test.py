from __future__ import division
import glob
import re
import string

from hmm import HMM, baum_welch

GENRES = {
    "a": "A. PRESS: REPORTAGE",
    "b": "B. PRESS: EDITORIAL",
    "c": "C. PRESS: REVIEWS",
    "d": "D. RELIGION",
    "e": "E. SKILL AND HOBBIES",
    "f": "F. POPULAR LORE",
    "g": "G. BELLES-LETTRES",
    "h": "H. MISCELLANEOUS: GOVERNMENT & HOUSE ORGANS",
    "j": "J. LEARNED",
    "k": "K: FICTION: GENERAL",
    "l": "L: FICTION: MYSTERY",
    "m": "M: FICTION: SCIENCE",
    "n": "N: FICTION: ADVENTURE",
    "p": "P. FICTION: ROMANCE",
    "r": "R. HUMOR"
}

train_sequences = list()

for genre in GENRES.keys():
    current_genre_docs_paths = glob.glob('brown/c%s*' % genre)
    for current_genre_doc_path in current_genre_docs_paths:
        current_genre_doc = open(current_genre_doc_path, 'r').read()

        current_genre_doc_words = re.findall(r'([a-zA-Z]+)/', current_genre_doc)
        train_sequences += [list(word.lower()) for word in current_genre_doc_words if len(word) > 1]


hmm_for_english_language = HMM(n_states=2, V=list(string.ascii_lowercase))
trained_hmm = baum_welch(hmm=hmm_for_english_language, Obs_seqs=train_sequences[:8000], epochs=50)

print '-' * 80
print 'initial state dist\t\t', trained_hmm.Pi[0], '\t\t', trained_hmm.Pi[1]
print '-' * 80
print 'observation\t\t\t\t\t', '\t\t\t\t\t'.join(['state0', 'state1'])
print '-' * 70
for observation, obs_prob_s0, obs_prob_s1 in zip(list(string.ascii_lowercase), trained_hmm.B[0], trained_hmm.B[1]):
    print observation, '\t\t\t\t\t', obs_prob_s0, '\t\t\t\t', obs_prob_s1, '\n', '-'*70, '\n'

print 'total', '\t\t\t\t', sum(trained_hmm.B[0]), '\t\t\t\t', sum(trained_hmm.B[1])
