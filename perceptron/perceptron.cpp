// Perceptron algorithm
// Consideration: linearly separable data
// Example: Boolean AND; outputs are binary (-1 or +1): 0 considered as -1 and 1 considered as +1

#include <iostream>
#include <fstream>

using namespace std;

const int number_of_training_data = 4, number_of_test_data = 4;
const int bool_dimension = 2;
const int data_dimension = bool_dimension + 1; // including bias

// Declaration and initialization of parameters
float learning_rate = 1;
float w[] = {0, 0, 0};


void allocate_data(int **&train_data, int *&train_labels, int **&test_data, int *&test_labels) {
    train_data = new int*[number_of_training_data];
    train_labels = new int[number_of_training_data];

    for(int i = 0; i < number_of_training_data; ++i)
        train_data[i] = new int[data_dimension];

    test_data = new int*[number_of_test_data];
    test_labels = new int[number_of_test_data];
    for(int i = 0; i < number_of_test_data; ++i)
        test_data[i] = new int[data_dimension];
}

void read_data(int **train_data, int *train_labels, int **test_data, int *test_labels) {
    // bias = 1, output 0 is replaced by -1
    train_data[0][0] = 1, train_data[0][1] = 0, train_data[0][2] = 0, train_labels[0] = -1;
    train_data[1][0] = 1, train_data[1][1] = 0, train_data[1][2] = 1, train_labels[1] = -1;
    train_data[2][0] = 1, train_data[2][1] = 1, train_data[2][2] = 0, train_labels[2] = -1;
    train_data[3][0] = 1, train_data[3][1] = 1, train_data[3][2] = 1, train_labels[3] = 1;

    test_data[0][0] = 1, test_data[0][1] = 0, test_data[0][2] = 0, test_labels[0] = -1;
    test_data[1][0] = 1, test_data[1][1] = 0, test_data[1][2] = 1, test_labels[1] = -1;
    test_data[2][0] = 1, test_data[2][1] = 1, test_data[2][2] = 0, test_labels[2] = -1;
    test_data[3][0] = 1, test_data[3][1] = 1, test_data[3][2] = 1, test_labels[3] = 1;
}

void save_trained_weights() {
    ofstream trained_weights("trained_weights.h");

    trained_weights << "#ifndef TRAINED_WEIGHTS_H\n#define TRAINED_WEIGHTS_H\n\n";
    trained_weights << "void set_weights() {\n";

    for (int i = 0; i < data_dimension; i++)
        trained_weights << "\tw_trained[" << i << "] = " << w[i] << ";\n";

    trained_weights << "}\n\n#endif\n";
    trained_weights.close();
}

int sign(float *weights, int *datum) {
    float result = 0;
    for (int i = 0; i < data_dimension; i++)
        result += weights[i]*datum[i];

    if (result < 0)
        return -1;
    else if (result > 0)
        return 1;

    return 0;
}

void train(int **data, int *labels) {
    cout << "---- train start\n";
    int correct_prediction_count = 0;
    int predicted_label;

    bool training_completed = false;

    // training
    cout << "----- weights -----\n";
    cout << w[0] << "\t" << w[1] << "\t" << w[2] << endl << endl;

    while (!training_completed) {
        for (int i = 0; i < number_of_training_data; i++) {
            predicted_label = sign(w, data[i]);

            if (predicted_label != labels[i]) {
                w[0] += learning_rate * labels[i] * data[i][0];
                w[1] += learning_rate * labels[i] * data[i][1];
                w[2] += learning_rate * labels[i] * data[i][2];
                cout << w[0] << "\t" << w[1] << "\t" << w[2] << endl << endl;
                correct_prediction_count = 0;
            } else {
                correct_prediction_count += 1;
            }

            if (correct_prediction_count == number_of_training_data) {
                training_completed = true;
                break;
            }

        }
    }

    save_trained_weights();

    cout << "-------------------\n";
    cout << "---- train end\nThe perceptron algorithm converged.\n\n";
}

void test(int **data, int *labels) {
    int predicted_label;

    cout << "---- testing\n";

    for (int i=0; i < number_of_test_data; i++) {
        predicted_label = sign(w, data[i]);
        cout << "(prediction, truth)\t(" << predicted_label << ", " << labels[i] << ")\n";
    }
}

void free_allocated_memory(int **train_data, int *train_labels, int **test_data, int *test_labels) {
    for(int i = 0; i < number_of_training_data; ++i)
        delete [] train_data[i];

    delete [] train_data;
    delete [] train_labels;

    for (int i=0; i<number_of_test_data; i++)
        delete [] test_data[i];

    delete [] test_data;
    delete [] test_labels;
}

int main() {
    int **train_data, **test_data;
    int *train_labels, *test_labels;

    allocate_data(train_data, train_labels, test_data, test_labels);
    read_data(train_data, train_labels, test_data, test_labels);

    train(train_data, train_labels);
    test(test_data, test_labels);

    free_allocated_memory(train_data, train_labels, test_data, test_labels);
    return 0;
}
