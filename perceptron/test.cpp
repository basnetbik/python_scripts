// Perceptron algorithm
// Consideration: linearly separable data
// Example: Boolean AND; outputs are binary (-1 or +1): 0 considered as -1 and 1 considered as +1

#include <iostream>
#include <fstream>

using namespace std;

const int number_of_test_data = 4;
const int bool_dimension = 2;
const int data_dimension = bool_dimension + 1; // including bias

// Declaration and initialization of parameters
float learning_rate = 1;
float w_trained[] = {0, 0, 0};

# include "trained_weights.h"

void allocate_data(int **&test_data, int *&test_labels) {
    test_data = new int*[number_of_test_data];
    test_labels = new int[number_of_test_data];
    for(int i = 0; i < number_of_test_data; ++i)
        test_data[i] = new int[data_dimension];
}

void read_data(int **test_data, int *test_labels) {
    // bias = 1, output 0 is replaced by -1
    test_data[0][0] = 1, test_data[0][1] = 0, test_data[0][2] = 0, test_labels[0] = -1;
    test_data[1][0] = 1, test_data[1][1] = 0, test_data[1][2] = 1, test_labels[1] = -1;
    test_data[2][0] = 1, test_data[2][1] = 1, test_data[2][2] = 0, test_labels[2] = -1;
    test_data[3][0] = 1, test_data[3][1] = 1, test_data[3][2] = 1, test_labels[3] = 1;
}

int sign(float *weights, int *datum) {
    float result = 0;
    for (int i = 0; i < data_dimension; i++)
        result += weights[i]*datum[i];

    if (result < 0)
        return -1;
    else if (result > 0)
        return 1;

    return 0;
}

void test(int **data, int *labels) {
    int predicted_label;

    cout << "---- testing\n";

    for (int i=0; i < number_of_test_data; i++) {
        predicted_label = sign(w_trained, data[i]);
        cout << "(prediction, truth)\t(" << predicted_label << ", " << labels[i] << ")\n";
    }
}

void free_allocated_memory(int **test_data, int *test_labels) {
    for (int i=0; i<number_of_test_data; i++)
        delete [] test_data[i];

    delete [] test_data;
    delete [] test_labels;
}

int main() {
    int **test_data, *test_labels;

    set_weights();
    allocate_data(test_data, test_labels);
    read_data(test_data, test_labels);

    test(test_data, test_labels);

    free_allocated_memory(test_data, test_labels);
    return 0;
}
