from collections import defaultdict
import glob
import pickle
import re


GENRES = {
    "a": "A. PRESS: REPORTAGE",
    "b": "B. PRESS: EDITORIAL",
    "c": "C. PRESS: REVIEWS",
    "d": "D. RELIGION",
    "e": "E. SKILL AND HOBBIES",
    "f": "F. POPULAR LORE",
    "g": "G. BELLES-LETTRES",
    "h": "H. MISCELLANEOUS: GOVERNMENT & HOUSE ORGANS",
    "j": "J. LEARNED",
    "k": "K: FICTION: GENERAL",
    "l": "L: FICTION: MYSTERY",
    "m": "M: FICTION: SCIENCE",
    "n": "N: FICTION: ADVENTURE",
    "p": "P. FICTION: ROMANCE",
    "r": "R. HUMOR"
}

GENRES_CONSIDERED = 15 #len(GENRES)   # len(GENRES) = 15


print('parsing data start\n')
words = defaultdict(list)
tags = defaultdict(list)
next_tags = defaultdict(list)

for genre in GENRES.keys()[:GENRES_CONSIDERED]:
    current_genre_docs_paths = glob.glob('../dictionary_search__/brown/c%s*' % genre)
    for current_genre_doc_path in current_genre_docs_paths:
        current_genre_doc = open(current_genre_doc_path, 'r').read().lower()
        sentences = filter(None, current_genre_doc.split('\n'))
        for sentence in sentences:
            cleaned_sentence = sentence.replace('\t', '')
            data = re.findall(r'([^/]+)/([^ ]+)', cleaned_sentence)

            states = [train_datum[1].strip() for train_datum in data]
            observations = [train_datum[0].strip() for train_datum in data]

            # Map states to integers and use the integers instead of state values
            # update state pairs with bigrams
            for i in range(1, len(states)):
                next_tags[(states[i-1],)].append(states[i])

            # update state with trigrams
            for i in range(2, len(states)):
                 next_tags[(states[i-2], states[i - 1])].append(states[i])

             # update state with four-grams
            for i in range(3, len(states)):
                 next_tags[(states[i-3], states[i - 2], states[i - 1])].append(states[i])


            for pos, word in zip(states, observations):
                if len(word) <= 1 and word not in ('a', 'i'):
                    continue

                words[word].append(pos)

                tags[pos].append(word)

print('parsing data end\n')

# for word in words:
#     words[word] = list(words[word])
#
#
# for tag in tags:
#     tags[tag] = list(tags[tag])
#
#
# for tag in next_tags:
#     next_tags[tag] = list(next_tags[tag])

with open('kbase_prob.pickle', 'wb') as f:
    pickle.dump(dict(words=words, tags=tags, next_tags=next_tags), f)
