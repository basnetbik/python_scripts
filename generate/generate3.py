"""
Random expression generator.
Actually, this acts as a probabilistic generator, as the items were saved redundantly the number of times they were
observed. However, it will take a lot of space.
To save the space: count the occurrences of the items and use probability instead.

Notice that loading the knowledge base might take some time.
And, in some run instances, these generators get stuck forever.

Also, there can be a lot of improvements with the expressions these generators are generating, one of which can be
using heuristics, such as, context and domain of the start word or the context of overall expression.
"""

import pickle
import random


with open('kbase_prob.pickle', 'rb') as kbase:
    kbase = pickle.load(kbase)
    words = kbase['words']  # information about the tags to which a word can belong to
    tags = kbase['tags']    # information about the words that can belong to the tag
    next_tags = kbase['next_tags']  # information about the tags that can follow the provided tag


def generate(start_word, length, result):
    if start_word not in words:
        return 'Word not in dictionary !!!'

    while True:
        tag = random.choice(words[start_word])
        if (tag,) in next_tags:
            selected_tags = [tag]
            break

    count = 1
    while count < length:
        while True:
            next_tag = random.choice(next_tags[tuple(selected_tags)])

            if tuple(selected_tags + [next_tag]) in next_tags and next_tag in tags:
                selected_tags += [next_tag]
                if len(selected_tags) == 3:
                    selected_tags.pop(0)

                break

        word = random.choice(tags[next_tag])
        result += ' ' + word
        count += 1


    print(result)

print('----- fishes -----')
generate('fishes', 5, 'fishes')
generate('do', 5, 'do')
generate('not', 5, 'not')
generate('fly', 5, 'fly')
generate('under', 5, 'under')
generate('most', 5, 'most')
generate('circumstances', 5, 'circumstances')

print('\n')
print('\n')

print('----- birds -----')
generate('some', 5, 'some')
generate('birds', 5, 'birds')
generate('swim', 5, 'swim')
