"""
Random expression generator.
"""

import pickle
import random


with open('kbase.pickle', 'rb') as kbase:
    kbase = pickle.load(kbase)
    words = kbase['words']  # information about the tags to which a word can belong to
    tags = kbase['tags']    # information about the words that can belong to the tag
    next_tags = kbase['next_tags']  # information about the tags that can follow the provided tag


def generate(start_word, length, result):
    if start_word not in words:
        return 'Word not in dictionary !!!'

    while True:
        tag = random.choice(words[start_word])
        if (tag,) in next_tags:
            break

    count = 1
    while count < length:
        while True:
            next_tag = random.choice(next_tags[(tag,)])

            if (next_tag,) in next_tags and next_tag in tags:
                break

        tag = next_tag
        word = random.choice(tags[next_tag])
        result += ' ' + word
        count += 1


    print(result)


generate('fish', 15, 'fish')
