# Perceptron Algorithm on Radially Separable Data

from __future__ import division
from cmath import polar
import matplotlib.pyplot as plt
from numpy import array, sign as np_sign


# 1 for bias, x+yj (eg: 0+0j) --> [1, x, y]
data = [
    [1, 50, 50],
    [1, 52, 52],
    [1, 54, 54],
    [1, 46, 54],
    [1, 52, 46],
    [1, 48, 50],
    [1, 53, 53],
    [1, 46, 46],
    [1, 46, 50],
    [1, 50, 54],
    [1, 50, 47],
    [1, 50, 51],
    [1, 50, 49],
    [1, 51, 50],
    [1, 49, 50],
    [1, 50, 52],
    [1, 50, 48],
    [1, 52, 50],
    [1, 54, 50]
]

labels = [
    1,
    1,
    -1,
    -1,
    -1,
    1,
    -1,
    -1,
    -1,
    -1,
    -1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    -1
]

color = ['b' if l == -1 else 'r' for l in labels]

# display data
B, X, Y = zip(*data)
plt.scatter(X, Y, c=color)
plt.title('Cartesian Coordinate System')

number_of_training_data = len(labels)
data_dimension = 2 + 1     # x,y + bias
learning_rate = 1


# Change of Origin (Centroid as new origin)
centroid_X = sum(X) / number_of_training_data
centroid_Y = sum(Y) / number_of_training_data


data = [
    [1, r, theta] for r, theta in map(lambda (b, x, y): polar(complex(x-centroid_X, y-centroid_Y)), data)
]


w = [0, 0, 0]


def sign(weights, datum):
    return np_sign(
        (
            sum(
                map(
                    lambda (weight, feature): weight*feature,
                    zip(weights, datum)
                )
            )
        )
    )


def train():
    print "---- train start\n"
    correct_prediction_count = 0
    training_completed = False

    global w
    print "----- weights -----\n"
    print w[0], "\t", w[1], "\t", w[2], "\n\n"

    iterations = 0
    while not training_completed:
        for i in range(number_of_training_data):
            (predicted_label) = sign(w, data[i])

            if predicted_label != labels[i]:
                w = [
                    w_feature + learning_rate * labels[i] * feature
                    for w_feature, feature in zip(w, data[i])
                ]

                print w[0], "\t", w[1], "\t", w[2], "\n\n"
                correct_prediction_count = 0
            else:
                correct_prediction_count += 1

            if correct_prediction_count == number_of_training_data:
                training_completed = True
                print "# iterations (to know that it converged): %d" % iterations
                print "# iterations (to actually converge): %d" % (iterations - number_of_training_data)
                break
            else:
                iterations += 1

    print "-------------------\n"
    print "---- train end\nThe perceptron algorithm converged.\n\n"


def test():
    print "---- testing\n"
    print "accuracy(%): ", (
        sum(
            [
                sign(w, datum) == label for datum, label in zip(data, labels)
            ]
        ) / number_of_training_data
    ) * 100


train()
test()

# display result
_, X, Y = zip(*data)
slope, intercept = -w[1]/w[2], -w[0]/w[2]

figure, axes = plt.subplots()
axes.scatter(X, Y, c=color)
X = array(plt.gca().get_xlim())
Y = intercept + slope * X
plt.plot(X, Y, 'g')
plt.title('Polar Coordinate System')

plt.show()
