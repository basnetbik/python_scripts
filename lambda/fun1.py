"""
Sort a list of data in the format 'priority2 priority1'
priority1 and priority2 are integers
"""

data = [
    '11 2', '3 4', '23 4', '3 4', '9 11'
]

def fun1(data_):
    return sorted(data_, key=lambda datum: (lambda datum_: (int(datum_[1]), int(datum_[0])))(datum.split(' ')))


print(fun1(data))
exec('print(fun1(data))')
